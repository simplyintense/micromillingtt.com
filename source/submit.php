<?php
require __DIR__ . '/config.php';

$type = @$_REQUEST['Type'];
$name = @$_REQUEST['Name'];
$email = @$_REQUEST['Email'];
$company = @$_REQUEST['Company'];
$phone = @$_REQUEST['Telephone'];
$country = @$_REQUEST['Country'];
$message = @$_REQUEST['Message'];

if (@trim($name) === '') {
  echo json_encode(array(
    'status' => 'fail',
    'message' => 'Enter your Name please.'
  ));
  exit;
}
if (@trim($email) === '') {
  echo json_encode(array(
    'status' => 'fail',
    'message' => 'Enter your Email please.'
  ));
  exit;
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  echo json_encode(array(
    'status' => 'fail',
    'message' => 'Invalid email address.'
  ));
  exit;
}
if (@trim($phone) === '') {
  echo json_encode(array(
    'status' => 'fail',
    'message' => 'Enter your Phone please.'
  ));
  exit;
}
if (@trim($company) === '') {
  echo json_encode(array(
    'status' => 'fail',
    'message' => 'Enter your Company please.'
  ));
  exit;
}
if (@trim($message) === '') {
  echo json_encode(array(
    'status' => 'fail',
    'message' => 'Enter your Message please.'
  ));
  exit;
}

$MAIL_TO = MAIL_TO_GENERAL;
if (@$type === 'Sales') {
  $MAIL_TO = MAIL_TO_SALES;
}

require __DIR__ . '/postmark.php';

$postmark = new Postmark(POSTMARK_TOKEN, MAIL_FROM);

$message = "<strong>Name:</strong> $name<br>".
"<strong>Email:</strong> $email<br>".
"<strong>Company:</strong> $company<br>".
"<strong>Telephone:</strong> $phone<br>".
"<strong>Country:</strong> $country<br>".
"<strong>Message:</strong> $message";

if($postmark->to($MAIL_TO)
    ->subject('Micro Milling Ltd. Contact Form Submission')
    ->html_message($message)
    ->send()){
  echo json_encode(array(
    'status' => 'ok',
    'message' => 'Thank you for contacting us. A member of our team will be in touch soon'
  ));
}